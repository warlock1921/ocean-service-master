import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../router.animations';
import $ from "jquery";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: [routerTransition()]
})
export class HomeComponent implements OnInit {

    constructor(public router: Router) {
    }

    ngOnInit() {
        // $('.details-tooltip').tooltip();
    }


    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

}
