import { OscCliUpdatePage } from './app.po';

describe('sb-admin-cli-update App', () => {
  let page: OscCliUpdatePage;

  beforeEach(() => {
    page = new OscCliUpdatePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
